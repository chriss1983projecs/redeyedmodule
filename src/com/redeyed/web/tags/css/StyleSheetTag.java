package com.redeyed.web.tags.css;

import java.io.IOException;

import javax.servlet.jsp.JspException;

import com.redeyed.web.tags.base.RedeyedBaseTag;

public class StyleSheetTag extends RedeyedBaseTag {
	private StringBuilder styleSheetTags = new StringBuilder();

	public void doTag() throws JspException, IOException {
		getJspWriter().println(getStyleSheetTags());
	}

	public StringBuilder getStyleSheetTags() throws IOException {
		// String styleSheet = "/resources/core/css/bootstrap.min.css";
		//add("/resources/core/css/hello.css");
		add("/resources/core/css/loginmodal.css");
		add("/resources/core/css/style.css");
		add("/resources/core/css/form-elements.css");
		add("/resources/bootstrap/css/bootstrap.min.css");

		return styleSheetTags;
	}

	private void add(String cssName) throws IOException {
		styleSheetTags.append("\n<link rel='stylesheet' media='all' type='text/css' href='" + getContextPath() + cssName + "' />");		
		//styleSheetTags.append("\n<link rel=\"stylesheet\" type=\"text/css\" href=\"<c:url value=\"" + getContextPath() + cssName + "\"/>\"/>");
	}
}
