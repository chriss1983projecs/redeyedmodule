package com.redeyed.web.tags.base;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.springframework.stereotype.Component;

/**
 * Base tag class with all base attributes and collections
 * 
 * @author Joby Pooppillikudiyil
 * @Since Mar 17, 2015
 * @Version Athena 1.0
 */
@Component
public abstract class RedeyedBaseTag extends SimpleTagSupport {

	
	protected JspWriter out;

	protected String contextPath;
	
	public String getContextPath() {
		if (contextPath == null) {
			contextPath = ((PageContext) getJspContext()).getServletContext().getContextPath();
			System.out.println("ContextPathc " + contextPath);
		}
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	public JspWriter getJspWriter() {
		if (out == null) {
			out = getJspContext().getOut();
		}
		return out;
	}

	protected boolean isHavingConfiguration() {
		return false;
	}
}
