package com.redeyed.web.tags.js;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import com.redeyed.web.tags.base.RedeyedBaseTag;

public class JavaScriptTag extends RedeyedBaseTag {
	protected String contextPath;
	private StringBuilder scriptTags = new StringBuilder();

	public void doTag() throws JspException, IOException {
		/*getJspWriter().println("<script>" + "var contextPath='" + getContextPath() + "';"
				+ "var DATE_FORMAT='MM/dd/yyyy';" + "var DATE_TIME_FORMAT='MM/dd/yyyy HH:mm';" + "</script>");
		getJspWriter().println(getScriptTags());*/
	}

	public StringBuilder getScriptTags() throws IOException {/*
		 add("/resources/core/js/jquery-1.11.1.js");
		 add("/resources/core/js/jquery-1.11.1.min.js");
		 add("/resources/core/js/jquery.backstretch.js");
		 add("/resources/core/js/jquery.backstretch.min.js");
		 add("/resources/core/js/jquery.backstretch.min.js");
		 add("/resources/core/js/placeholder.js");
		 add("/resources/core/js/scripts.js");*/
		 
//		add("/resources/core/js/jquery/jquery-1.11.2.js");
//		
//		add("/resources/lib/form/js/form-helper.js");

		return scriptTags;
	}

	private void add(String jsLibName) throws IOException {
		scriptTags.append("\n<script src='" + getContextPath() + jsLibName + "' type='text/javascript'></script>");
	}

	public String getContextPath() {
		if (contextPath == null) {
			contextPath = ((PageContext) getJspContext()).getServletContext().getContextPath();
		}
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	public JspWriter getJspWriter() {
		if (out == null) {
			out = getJspContext().getOut();
		}
		return out;
	}

}
