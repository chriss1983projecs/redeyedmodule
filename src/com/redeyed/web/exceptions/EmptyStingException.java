package com.redeyed.web.exceptions;

public class EmptyStingException extends Exception {
	public EmptyStingException(String message) {
		super(message);
	}
}