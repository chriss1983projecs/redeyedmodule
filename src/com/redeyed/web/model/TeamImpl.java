package com.redeyed.web.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.log4j.Logger;

@Entity
@Table(name="TEAM", 
	   uniqueConstraints={@UniqueConstraint(columnNames={"ID"})})
public class TeamImpl implements Team, Serializable, IModel {
	final static Logger logger = Logger.getLogger(TeamImpl.class);
	
	private static final long serialVersionUID = -9033527683159348260L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", nullable=false, unique=true, length=11)
	private int id;
	@ElementCollection
	@CollectionTable(name="PLAYERS_MAPPING")
	@Column(name="PLAYERS", length=20, nullable=true)
	private List<Integer> idPlayers;
	
	@Column(name="TEAMNAME", length=100, nullable=true)
	private String teamName;
	
	@Column(name = "POINTSSERIESONE", length = 10, nullable = true)
	private int pointsSeriesOne;
	@Column(name = "POINTSSERIESTWO", length = 10, nullable = true)
	private int pointsSeriesTwo;
	@Column(name = "TOTALSCORE", length = 10, nullable = true)
	private int totalScore;
	
	public TeamImpl() {
//		idPlayers = new ArrayList<Integer>();
	}

	public List<Integer> getIdPlayers() {
		return idPlayers;
	}

	public void setIdPlayers(List<Integer> idPlayers) {
		this.idPlayers = idPlayers;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public int getPointsSeriesOne() {
		return pointsSeriesOne;
	}

	public void setPointsSeriesOne(int pointsSeriesOne) {
		this.pointsSeriesOne = pointsSeriesOne;
	}

	public int getPointsSeriesTwo() {
		return pointsSeriesTwo;
	}

	public void setPointsSeriesTwo(int pointsSeriesTwo) {
		this.pointsSeriesTwo = pointsSeriesTwo;
	}

	public int getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}

	public int getId() {
		return id;
	}


	@Override
	public String toString() {
		return "TeamImpl [id=" + id + ", idPlayers=" + idPlayers + ", teamName=" + teamName + ", pointsSeriesOne="
				+ pointsSeriesOne + ", pointsSeriesTwo=" + pointsSeriesTwo + ", totalScore=" + totalScore + "]";
	}
	
	
}
