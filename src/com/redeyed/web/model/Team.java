package com.redeyed.web.model;

import java.util.List;

public interface Team {

	public List<Integer> getIdPlayers();

	public void setIdPlayers(List<Integer> idPlayers);

	public String getTeamName();

	public void setTeamName(String teamName);

	public int getPointsSeriesOne();

	public void setPointsSeriesOne(int pointsSeriesOne);

	public int getPointsSeriesTwo();

	public void setPointsSeriesTwo(int pointsSeriesTwo);

	public int getTotalScore();

	public void setTotalScore(int totalScore);

	public int getId();
}
