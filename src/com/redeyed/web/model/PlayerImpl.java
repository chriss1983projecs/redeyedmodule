package com.redeyed.web.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.log4j.Logger;
import org.apache.taglibs.standard.tag.rt.core.SetTag;

@Entity
@Table(name="PLAYER", 
	   uniqueConstraints={@UniqueConstraint(columnNames={"ID"})})
public class PlayerImpl implements Player, Serializable, IModel {
	final static Logger logger = Logger.getLogger(PlayerImpl.class);
	
	private static final long serialVersionUID = -9033527683159348260L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", nullable=false, unique=true, length=11)
	private int playerID;
	@Column(name="STARTNUMBER", length=10, nullable=true)
	private int startNumber;
	@Column(name="PAID", length=20, nullable=true)
	private boolean paid = false;
	@Column(name="FIRSTNAME", length=20, nullable=true)
	private String firstName;
	@Column(name="LASTNAME", length=20, nullable=true)
	private String lastName;
	@Column(name="HOUSENUMBER", length=20, nullable=true)
	private String houseNumber;
	@Column(name="STREET", length=20, nullable=true)
	private String street;
	@Column(name="ZIPCODE", length=20, nullable=true)
	private String zipCode;
	@Column(name="CITY", length=20, nullable=true)
	private String city;
	@Column(name="PLAYERINFO", length=255, nullable=true)
	private String playerInfo;
	@Column(name="EMAIL", length=55, nullable=true)
	private String eMail;
	@Column(name="MOBILEPHONENUMBER", length=20, nullable=true)
	private String mobilePhoneNumber;
	@Column(name="PHONE", length=20, nullable=true)
	private String phone;
	
	@Column(name = "POINTSSERIESONE", length = 10, nullable = true)
	private int pointsSeriesOne;
	@Column(name = "POINTSSERIESTWO", length = 10, nullable = true)
	private int pointsSeriesTwo;
	@Column(name = "TOTALSCORE", length = 10, nullable = true)
	private int totalScore;
	
	public PlayerImpl() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @return this. the startNummer
	 */
	public int getStartNumber() {
		return this.startNumber;
	}
	/**
	 * @param startNummer the startNummer to set
	 */
	public void setStartNumber(int startNumber) {
		this.startNumber = startNumber;
	}
	/**
	 * @return this. the paid
	 */
	public boolean isPaid() {
		return this. paid;
	}
	/**
	 * @param paid the paid to set
	 */
	public void setPaid(boolean paid) {
		this.paid = paid;
	}
	/**
	 * @return this. the firstName
	 */
	public String getFirstName() {
		return this. firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return this. the lastName
	 */
	public String getLastName() {
		return this.lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return this. the houseNumber
	 */
	public String getHouseNumber() {
		return this.houseNumber;
	}
	/**
	 * @param houseNumber the houseNumber to set
	 */
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	/**
	 * @return this. the street
	 */
	public String getStreet() {
		return this.street;
	}
	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	/**
	 * @return this. the zipCode
	 */
	public String getZipCode() {
		return this.zipCode;
	}
	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	/**
	 * @return this. the city
	 */
	public String getCity() {
		return this.city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return this. the playerInfo
	 */
	public String getPlayerInfo() {
		return this. playerInfo;
	}
	/**
	 * @param playerInfo the playerInfo to set
	 */
	public void setPlayerInfo(String playerInfo) {
		this.playerInfo = playerInfo;
	}
	
	/**
	 * @return this. the eMail
	 */
	public String getEMail() {
		return this.eMail;
	}

	/**
	 * @param eMail the eMail to set
	 */
	public void setEMail(String eMail) {
		this.eMail = eMail;
	}

	/**
	 * @return this. the mobilePhoneNumber
	 */
	public String getMobilePhoneNumber() {
		return this.mobilePhoneNumber;
	}

	/**
	 * @param mobilePhoneNumber the mobilePhoneNumber to set
	 */
	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	/**
	 * @return this. the phone
	 */
	public String getPhone() {
		return this.phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return this. the id
	 */
	public int getPlayerID() {
		// TODO Auto-generated method stub
		return 0;
	}

	public String geteMail() {
		return this.eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public int getPointsSeriesOne() {
		return this.pointsSeriesOne;
	}

	public void setPointsSeriesOne(int pointsSeriesOne) {
		this.pointsSeriesOne = pointsSeriesOne;
	}

	public int getPointsSeriesTwo() {
		return this. pointsSeriesTwo;
	}

	public void setPointsSeriesTwo(int pointsSeriesTwo) {
		this.pointsSeriesTwo = pointsSeriesTwo;
	}

	public int getTotalScore() {
		return this. totalScore;
	}

	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}

	@Override
	public String toString() {
		return "PlayerImpl [id=" + playerID + ", startNumber=" + startNumber + ", paid=" + paid + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", houseNumber=" + houseNumber + ", street=" + street + ", zipCode="
				+ zipCode + ", city=" + city + ", playerInfo=" + playerInfo + ", eMail=" + eMail
				+ ", mobilePhoneNumber=" + mobilePhoneNumber + ", phone=" + phone + ", pointsSeriesOne="
				+ pointsSeriesOne + ", pointsSeriesTwo=" + pointsSeriesTwo + ", totalScore=" + totalScore + "]";
	}

}
