package com.redeyed.web.model;

public interface Player {
	
	public int getStartNumber();

	public void setStartNumber(int startNummer);

	public boolean isPaid();

	public void setPaid(boolean paid);

	public String getFirstName();

	public void setFirstName(String firstName);

	public String getLastName();

	public void setLastName(String lastName);

	public String getHouseNumber();

	public void setHouseNumber(String houseNumber);

	public String getStreet();

	public void setStreet(String street);

	public String getZipCode();

	public void setZipCode(String zipCode);

	public String getCity();

	public void setCity(String city);

	public String getPlayerInfo();

	public void setPlayerInfo(String playerInfo);

	public String getEMail();

	public void setEMail(String eMail);

	public String getMobilePhoneNumber();

	public void setMobilePhoneNumber(String mobilePhoneNumber);

	public String getPhone();

	public void setPhone(String phone);

	public int getPlayerID();

	public int getPointsSeriesOne();

	public void setPointsSeriesOne(int pointsSeriesOne);

	public int getPointsSeriesTwo();

	public void setPointsSeriesTwo(int pointsSeriesTwo);

	public int getTotalScore();

	public void setTotalScore(int totalScore);

	public String toString();
}
