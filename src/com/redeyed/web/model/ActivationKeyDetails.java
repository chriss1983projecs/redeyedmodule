package com.redeyed.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="AKTIVATION_KEY_DETAILS", 
	   uniqueConstraints={@UniqueConstraint(columnNames={"ID"})})
public class ActivationKeyDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true, length = 11)
	private Integer userLoginInfoId;

	@Column(name = "CREATION_DATE", nullable = true)
	private Date creationDate;

	@Column(name = "EXPIRE_DATE", nullable = true)
	private Date expireDate;

	@Column(name = "VALID", length = 30, nullable = true)
	private Boolean valid;

	@Column(name = "CRYPTED_AKTIVATION_KEY", length = 60, nullable = true)
	private String cryptedAktivationKey;

	@Column(name = "USERID", length = 30, nullable = true)
	private Integer userId;

	public Integer getUserLoginInfoId() {
		return userLoginInfoId;
	}

	public void setUserLoginInfoId(Integer userLoginInfoId) {
		this.userLoginInfoId = userLoginInfoId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	public String getCryptedAktivationKey() {
		return cryptedAktivationKey;
	}

	public void setCryptedAktivationKey(String cryptedAktivationKey) {
		this.cryptedAktivationKey = cryptedAktivationKey;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "UserLoginInfoEntity [userLoginInfoId=" + userLoginInfoId + ", creationDate=" + creationDate
				+ ", expireDate=" + expireDate + ", valid=" + valid + ", cryptedAktivationKey=" + cryptedAktivationKey
				+ ", userId=" + userId + "]";
	}

}