package com.redeyed.web.security;

import java.math.BigInteger;
import java.util.Random;


public class SecurityHandler {
	private static final BigInteger E = BigInteger.valueOf(3L);

	final Random r = new Random();

	public SecurityHandler() {
	}
	
	public static class KeyPair {

		public final BigInteger publicKey;
		public final BigInteger privateKey;

		public KeyPair(final BigInteger publicKey, final BigInteger privateKey) {
			this.publicKey = publicKey;
			this.privateKey = privateKey;
		}
	}

	public KeyPair genKeyPair() {

		int nTries = 0;
		do {
			try {
				boolean hasPrime = false;

				BigInteger p = BigInteger.ZERO;
				while (!hasPrime) {
					p = BigInteger.probablePrime(28, r);
					hasPrime = p.isProbablePrime(8);
				}

				hasPrime = false;
				BigInteger q = BigInteger.ZERO;
				while (!hasPrime) {
					q = BigInteger.probablePrime(28, r);
					hasPrime = p.isProbablePrime(8) && !(q.equals(p));
				}

				final BigInteger n = p.multiply(q);

				final BigInteger eTot = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
				final BigInteger d = E.modInverse(eTot);

				return new KeyPair(n, d);
			} catch (final ArithmeticException ex) {
			}
		} while (nTries++ < 20);

		throw new RuntimeException("Konnte kein Schluesselpaar erzeugen.");
	}

	public BigInteger hash(final BigInteger number) {
		return number.mod(BigInteger.valueOf(837951584));
	}

	public BigInteger sign(final BigInteger number, final KeyPair kp) {
		return hash(number).modPow(kp.privateKey, kp.publicKey);
	}

	public boolean verify(final BigInteger message, final BigInteger signature, final BigInteger publicKey) {
		return hash(message).equals(signature.modPow(E, publicKey));
	}

	public String generateSerial(final KeyPair kp, final Random r) {
		int nTries = 0;
		do {
			final BigInteger message = BigInteger.probablePrime(28, r);
			if (message.toString(36).length() != 6) {
				continue;
			}

			final BigInteger sign = sign(message, kp);
			if (sign.toString(36).length() != 10) {
				continue;
			}

			return (message.toString(36) + sign.toString(36)).toUpperCase();
		} while (nTries++ < 100);

		throw new RuntimeException("Konnte keine Seriennnummer generieren");
	}

	public boolean verify(final String serial, final BigInteger publicKey) {
		return verify(new BigInteger(serial.substring(0, 6), 36), new BigInteger(serial.substring(6, 16), 36),
				publicKey);
	}

	public static void main(String[] args) {

		final SecurityHandler s = new SecurityHandler();

		
		final KeyPair kp = s.genKeyPair();
		System.out.println(String.format("Ein Schlüsselpaar...\n%1$s:%2$s\n%3$s:%4$s", "private", kp.privateKey,
				"public", kp.publicKey));

		final Random r = new Random();

		System.out.println("\nEin paar Seriennummern...");
		for (int i = 0; i < 20; i++) {
			final String serial = s.generateSerial(kp, r);

			System.out.println(serial + (s.verify(serial, kp.publicKey) ? " (ok)" : " NOK!"));
		}
		System.out.println();

		char[] str = "44S0-PVMW-GXQ9-ACT5".toCharArray();
		String newStr = "";

		for (int i = 0; i < str.length; i++) {
			if (i != 0) {
				if (str[i] == '-') {
					i++;
					System.out.print("-");
				}
			}
			newStr += str[i];
		}
		System.out.println("Out = " + newStr);
		System.out.println("36559548014518483" + (s.verify("2NCVIPDZS42P0C3I", BigInteger.valueOf(35708814176160043L) ) ? " (ok)" : " NOK!"));
		
		KeyPair p = new KeyPair(BigInteger.valueOf(35057721672261313L), BigInteger.valueOf(23371814198046179L));
		
		String key = s.generateSerial(p, new Random());
		
		System.out.println("NEW KEYS: " + key);
		System.out.println(s.verify(key, BigInteger.valueOf(35057721672261313L)));

//		 System.out.println(str.activationCode.substring(0, 4) + "-" + str.substring(4, 8) +
//		 "-" + str.substring(8, 12) + "-" + str.substring(12, 16));
//		 String key = str.replace("-", "");
//		 System.out.println(key2T50GH7YYNNE1PVW);

		// System.out.println(s.verify("2EHMX56IXJT3FX4S",
		// BigInteger.valueOf(40094985638641183L)));
		/*
		Ein Schlüsselpaar...
		private:19423305581331139
		public:29134958722527793

		Ein paar Seriennummern...
		2OVSS1V50ACU17D5 (ok)
		2O4RCPN4RBMF0TK1 (ok)
		2D9SKPURF5PJLGBC (ok)
		37HONNE8DVFP59S3 (ok)
		2OUJTPDM81UC4EPI (ok)
		3X5ONT29EAD973C8 (ok)
		3TIMSH2XCM0TCXLW (ok)
		44S2OHEB0WE2STZ2 (ok)
		38P80DDQ80ATLQM8 (ok)
		2EFOS532EZCR1FKS (ok)
		2JX2TJNLURGPPMA7 (ok)
		4C1441EBBG4FF1KM (ok)
		4EEO0PX2MLKCBIIG (ok)
		3Z6QVNZ8QBFTY7QS (ok)
		47EHPP7NG97QZC7S (ok)
		3YCZ05HY6UGVHV79 (ok)
		2EIA4ZJ1LQSXVSNI (ok)
		2CRKXTPTQWQHTRH0 (ok)
		3GFD91DT5JGFHMXD (ok)
		35A7FDQIADQR3OVL (ok)

		---Out = 44S0PVMWGXQ9ACT5
		36559548014518483 (ok)
		NEW KEYS: 3JM1B1GHAH1H6W6Q
		true
*/
	}
}
