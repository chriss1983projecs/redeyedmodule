package com.redeyed.web.utils;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import org.apache.log4j.Logger;

public class ReflectionUtils {
	final static Logger logger = Logger.getLogger(ReflectionUtils.class);
	public static Set<Class> getAllClasses(String pckgname) {
		try {
			Set<Class> classes = new HashSet<Class>();
			// Get a File object for the package
			File directory = null;
			try {
				directory = new File(Thread.currentThread().getContextClassLoader()
						.getResource(pckgname.replace('.', '/')).getFile());
			} catch (NullPointerException x) {
				logger.error("Get Nullpointer " + x.getMessage());
				throw new ClassNotFoundException(pckgname + " does not appear to be a valid package");
			}
			if (directory.exists()) {
				// Get the list of the files contained in the package
				String[] files = directory.list();
				for (int i = 0; i < files.length; i++) {
					// we are only interested in .class files
					if (files[i].endsWith(".class")) {
						// removes the .class extension
						classes.add(Class.forName(pckgname + '.' + files[i].substring(0, files[i].length() - 6)));
					}
				}
			} else {
				logger.debug("Package does not exist");
				throw new ClassNotFoundException(pckgname + " does not appear to be a valid package");
			}

			return classes;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Set<Class> getClasseNamesInPackage(String jarName, String packageName){
	   Set<Class> classes = new HashSet<Class>();

	   packageName = packageName.replaceAll("\\." , "/");
	   //if (debug)
		 System.out.println ("Jar " + jarName + " looking for " + packageName);
	   try {
	     JarInputStream jarFile = new JarInputStream
	        (new FileInputStream (jarName));
	     JarEntry jarEntry;

	     while(true) {
	       jarEntry=jarFile.getNextJarEntry ();
	       if(jarEntry == null){
	         break;
	       }
	       if((jarEntry.getName ().startsWith (packageName)) &&
	            (jarEntry.getName ().endsWith (".class")) ) {
	         //if (debug) System.out.println("Found " + jarEntry.getName().replaceAll("/", "\\."));
	         classes.add(jarEntry.getName().replaceAll("/", "\\.").getClass());
	       }
	     }
	   }
	   catch( Exception e){
	     e.printStackTrace ();
	   }
	   return classes;
	}
}
