package com.redeyed.web.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

//import com.skat_haan_gruiten.manager.properties.Properties;

public class JSONPropertiesManager {
	private final static Logger logger = Logger.getLogger(JSONPropertiesManager.class);
	private ObjectMapper mapper = new ObjectMapper();
	private static JSONPropertiesManager instance = null;
	private Map<String, Object> map;

	public JSONPropertiesManager() {
		map = new HashMap<String, Object>();
		try {
			logger.debug("Load Properties File");
			map = mapper.readValue(new File("resources/properties.json"), new TypeReference<Map<String, Object>>() {
			});

		} catch (IOException e) {
			logger.error("Propertie JSON File not found! " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getMap(String key) {
		return (Map<String, Object>) map.get(key);
	}

	public Map<String, Object> getGerneralConfig() {
		return map;
	}

	public void setMap(String key, Object value) {
		map.put(key, value);
	}

	public static JSONPropertiesManager getProperty() {
		if(instance == null) {
			instance = new JSONPropertiesManager();
		}
		return instance;
	}

	public static void main(String[] args) {
		System.out.println(new JSONPropertiesManager().getMap("database").get("databaseURL"));
	}

	

}
