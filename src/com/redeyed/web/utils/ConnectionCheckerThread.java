package com.redeyed.web.utils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;


public class ConnectionCheckerThread implements Runnable {
	
	public ConnectionCheckerThread() {
		Thread connectionChecker = new Thread(this);
		connectionChecker.start();
	}
	
	public void run() {
		while(true) {
			Socket sock = new Socket();
		    InetSocketAddress addr = new InetSocketAddress("google.de",80);
		    try {
		        sock.connect(addr,3000);
		        System.out.println("Set Online");
		        
		    } catch (IOException e) {
		    	System.out.println("Set Offline");
		    } finally {
		        try {sock.close();}
		        catch (IOException e) {}
		    }
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
