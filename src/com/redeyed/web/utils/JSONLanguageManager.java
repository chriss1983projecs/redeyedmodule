package com.redeyed.web.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

//import com.skat_haan_gruiten.manager.properties.Properties;

public class JSONLanguageManager {
	private final static Logger logger = Logger.getLogger(JSONLanguageManager.class);
	private ObjectMapper mapper = new ObjectMapper();
	private static JSONLanguageManager instance = null;
	private Map<String, Object> map;

	private JSONLanguageManager() {
		map = new HashMap<String, Object>();
		try {
			logger.debug("Load Properties File");
			map = mapper.readValue(new File("/lang/" + Locale.getDefault().getLanguage() +".json"), new TypeReference<Map<String, Object>>() {
			});

		} catch (IOException e) {
			logger.error("Propertie JSON File not found! " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public Map<String, Map<String, Map<String, Object>>> getMap(String key) {
		return (Map<String, Map<String, Map<String, Object>>>) map.get(key);
	}

	public Map<String, Object> getGerneralConfig() {
		return map;
	}

	public void setMap(String key, Object value) {
		map.put(key, value);
	}

	public static JSONLanguageManager getProperty() {
		if(instance == null) {
			instance = new JSONLanguageManager();
		}
		return instance;
	}

	public static void main(String[] args) {
		System.out.println(Locale.getDefault().getLanguage());
		System.out.println(JSONLanguageManager.getProperty().getMap("menubar").get("filemenu").get("newmenu").get("adduser") + "");
	}

}
