package com.redeyed.web.i18n;

import java.util.HashMap;
import java.util.Map;

public class I18N {
	private static I18N instance;
	
	public static String de_403 = "Das öffnen dieser Seite ist Ihnen nicht erlaubt";
	public static String en_403 = "That opened this page is not allowed";
	public static String de_404 = "Die von Ihnen angeforderte Seite konnte nicht gefunden werden, entweder wenden Sie sich an Ihren Webmaster oder versuchen Sie es erneut.";
	public static String en_404 = "The page you requested could not be found, either contact your Webmaster or try again.";
	
	private static Map<String, Object> stings;
	
	private I18N() {
		stings  = new HashMap<String, Object>();
		initStrings();
	}
	
	private void initStrings() {
		stings.put("de_403", de_403);
		stings.put("en_403", en_403);
		stings.put("de_404", de_404);
		stings.put("en_404", en_404);
	}
	
	public static String getString(String currentLocale) {
		System.out.println("Get String " + currentLocale);
		if(instance == null) {
			System.out.println("new Instance");
			instance = new I18N();
		}
		
		return stings.get(currentLocale).toString();
	}
}
