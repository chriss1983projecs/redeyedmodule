package com.redeyed.web.controller;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.redeyed.web.database.RedeyedCoreManagerDao;
import com.redeyed.web.i18n.I18N;
import com.redeyed.web.model.ActivationKeyDetails;
import com.redeyed.web.model.Player;
import com.redeyed.web.model.PlayerImpl;
import com.redeyed.web.model.UserDetailsEntity;
import com.redeyed.web.pojos.RequestObj;
import com.redeyed.web.security.CryptUtil;
import com.redeyed.web.security.SecurityHandler;
import com.redeyed.web.security.SecurityHandler.KeyPair;
import com.redeyed.web.service.DefaultService;
import com.redeyed.web.service.MailService;

@EnableWebMvc
@Controller
public class RedeyedController {

	private final Logger logger = LoggerFactory.getLogger(RedeyedController.class);
	private final DefaultService defaultService;
	protected static Gson GSON = new GsonBuilder().create();

	private SecurityHandler securityHandler = new SecurityHandler();

	private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

	private final BigInteger publicKey = BigInteger.valueOf(29134958722527793L);

	@Autowired
	public RedeyedController(DefaultService helloWorldService) {
		this.defaultService = helloWorldService;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index(Map<String, Object> model) {
		
		logger.debug("index() is executed!");

		Player p = new PlayerImpl();
		p.setFirstName("Günter");
		p.setLastName("Maier");
		p.setZipCode("40822");
		p.setCity("Mettmann");

		model.put("title", defaultService.getTitle(""));
		model.put("msg", defaultService.getDesc());
		model.put("player", p);

		ModelAndView modelAndView = new ModelAndView("index");
		
		return modelAndView;
	}

	@RequestMapping(value = "/hello/{name:.+}", method = RequestMethod.POST)
	public ModelAndView hello(@PathVariable("name") String name) {

		logger.debug("hello() is executed - $name {}", name);

		ModelAndView model = new ModelAndView();
		model.setViewName("index");

		model.addObject("title", defaultService.getTitle(name));
		model.addObject("msg", defaultService.getDesc());

		return model;
	}

	@RequestMapping(value = "/data/keyagreement", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public @ResponseBody String keyHandlerPOST(@RequestBody RequestObj requestObj, HttpServletRequest request,
			HttpServletResponse response) throws JsonSyntaxException, Exception {
		JSONObject uiObj = new JSONObject();
		if (requestObj.getRequestType().equals("Z8LSq0wWwB5v33481384")) {

			String key = "Z8LSq0wWwB5v+6YJzurcP463H3F12iZh74fDj4S74oUH4EONkiKb2FmiWUbtFh97GG/c/lbDE47mvw6j94yXxKHOpoqu6zpLKMKPcOoSppcVWb2q34qENBJkudXUh4MWcreondLmLL2UyydtFKuU9Sa5VgY/CzGaVGJABK2ZR94=";
			String keyAgreement = null;

			keyAgreement = GSON.fromJson(CryptUtil.decrypt(requestObj.getParams().get("GET_KEY").toString(), key),
					String.class);
			System.out.println("keyAgreement -> " + keyAgreement);

			logger.debug(
					"keyAgreement.equals(\"Z8LSq0wWwB5v33481384\") = " + keyAgreement.equals("Z8LSq0wWwB5v33481384"));
			if (keyAgreement.equals("Z8LSq0wWwB5v33481384")) {
				logger.debug("MATCH");
				uiObj.put("KEY_AGREEMENT", true);
				uiObj.put("ACTIVATION_STATE", "OK");
			} else {
				uiObj.put("KEY_AGREEMENT", false);
				uiObj.put("ACTIVATION_STATE", "NOK");
			}

			logger.debug("ACTIVATION_CODE = " + requestObj.getParams().get("ACTIVATION_CODE"));
		}
		return uiObj.toString();
	}

	@RequestMapping(value = "/data/activator", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public @ResponseBody String dataProviderPOST(@RequestBody RequestObj requestObj, HttpServletRequest request,
			HttpServletResponse response, Map<String, Object> model) throws Exception {
		if (requestObj.getRequestType().equals("Z8LSq0wWwB5v33481384")) {
			String encryptionKey = "Z8LSq0wWwB5v+6YJzurcP463H3F12iZh74fDj4S74oUH4EONkiKb2FmiWUbtFh97GG/c/lbDE47mvw6j94yXxKHOpoqu6zpLKMKPcOoSppcVWb2q34qENBJkudXUh4MWcreondLmLL2UyydtFKuU9Sa5VgY/CzGaVGJABK2ZR94=";
			String activationCode = null;
			JSONObject uiObj = new JSONObject();
			String newStr = "";
			activationCode = GSON.fromJson(
					CryptUtil.decrypt((String) requestObj.getParams().get("ACTIVATION_CODE"), encryptionKey),
					String.class);
			activationCode = activationCode.replaceAll("\\s", "");
			if (activationCode.contains("-")) {
				char[] str = activationCode.toCharArray();

				for (int i = 0; i < str.length; i++) {
					if (i != 0) {
						if (str[i] == '-') {
							i++;
							System.out.print("-");
						}
					}
					newStr += str[i];
				}
			}

			activationCode = newStr.isEmpty() ? activationCode : newStr;

			if (newStr.length() != 15 && securityHandler.verify(activationCode, publicKey)) {
				uiObj.put("ACTIVATION_STATE", "OK");
				logger.debug("ACTIVATION_STATE = OK");
			} else {
				uiObj.put("ACTIVATION_STATE", "FAIL");
				logger.debug("ACTIVATION_STATE = NOK");
			}

			return uiObj.toString();

		} else {
			try {
				model.put("msg_403", I18N.getString(Locale.getDefault().getLanguage() + "_403").toString());

			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("Set Lang to " + Locale.getDefault().getLanguage());
			return "code/403";
		}
	}

	@RequestMapping(value = "/skatmanager/dataProvider", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public @ResponseBody String skatDataProviderPOST(@RequestBody RequestObj requestObj, HttpServletRequest request,
			HttpServletResponse response) throws JsonSyntaxException, Exception {
		JSONObject uiObj = new JSONObject();

		logger.debug("Request Object", requestObj.toString());

		UserDetailsEntity user = new UserDetailsEntity();
		

		if (requestObj.getRequestType().equals("Z8LSq0wWwB5v+6YJzurc")) {
			ModelAndView model = new ModelAndView();
			model.setViewName("index");

			String key = "Z8LSq0wWwB5v+6YJzurcP463H3F12iZh74fDj4S74oUH4EONkiKb2FmiWUbtFh97GG/c/lbDE47mvw6j94yXxKHOpoqu6zpLKMKPcOoSppcVWb2q34qENBJkudXUh4MWcreondLmLL2UyydtFKuU9Sa5VgY/CzGaVGJABK2ZR94=";
			user = GSON.fromJson(CryptUtil.decrypt((String) requestObj.getParams().get("USER"), key),
					UserDetailsEntity.class);

			System.out.println("FirstName = " + user.getFirstName() + "\nLastName = " + user.getLastName()
					+ "\nEmail = " + user.getEmail());

			// user.setFirstName("Maria");

			uiObj.put("USER_DETAILS", GSON.toJson(user));
			response.setStatus(HttpServletResponse.SC_OK);

			return uiObj.toString();
		} else {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);

			return "code/403";
		}
	}

	@RequestMapping(value = "/skatmanager/getActivationCode", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public @ResponseBody String skatgetActivationCode(@RequestBody RequestObj requestObj, HttpServletRequest request,
			HttpServletResponse response) throws JsonSyntaxException, Exception {
		JSONObject uiObj = new JSONObject();
		
		if (requestObj.getRequestType().equals("Z8LSq0wWwB5v+6YJzurc")) {
			ModelAndView model = new ModelAndView();
			String key = "Z8LSq0wWwB5v+6YJzurcP463H3F12iZh74fDj4S74oUH4EONkiKb2FmiWUbtFh97GG/c/lbDE47mvw6j94yXxKHOpoqu6zpLKMKPcOoSppcVWb2q34qENBJkudXUh4MWcreondLmLL2UyydtFKuU9Sa5VgY/CzGaVGJABK2ZR94=";
			model.setViewName("index");
			
			UserDetailsEntity user = GSON.fromJson(CryptUtil.decrypt((String) requestObj.getParams().get("USER"), key),
					UserDetailsEntity.class);

//			String activationKey = CryptUtil.encrypt(generateNewSerial(), key);
			
			ActivationKeyDetails activationKeyDetails = new ActivationKeyDetails();
			
			activationKeyDetails.setCreationDate(new Date());
//			activationKeyDetails.setCryptedAktivationKey(activationKey);
			activationKeyDetails.setExpireDate(new Date());
			activationKeyDetails.setValid(true);
			activationKeyDetails.setUserId(user.getUserDetailsId());
			
//			UserDetailsEntity u = (UserDetailsEntity) DataBaseUtil.getUtil().selectAll(UserDetailsEntity.class);

//			MailService.sendMail(user.getEmail(), activationKey);
			uiObj.put("EMAIL_STATE", GSON.toJson("OK"));
			
			response.setStatus(HttpServletResponse.SC_OK);
			
			RedeyedCoreManagerDao.getDataBaseUtil().saveOrUpdate(activationKeyDetails);
			RedeyedCoreManagerDao.getDataBaseUtil().saveOrUpdate(user);

			return uiObj.toString();
		} else {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);

			return "code/403";
		}
	}
/*
	// redirect all unhandled pages to the 404 page
	@RequestMapping(value = "/**")
	public String Error(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {

		try {
			model.put("msg_404", I18N.getString(Locale.getDefault().getLanguage() + "_404").toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Set Lang to " + Locale.getDefault().getLanguage());

		return "code/404";
	}

	private String generateNewSerial() {
		SecurityHandler s = new SecurityHandler();
		KeyPair p = new KeyPair(BigInteger.valueOf(35057721672261313L), BigInteger.valueOf(23371814198046179L));

		return s.generateSerial(p, new Random());

		// System.out.println("NEW KEYS: " + key);
		// System.out.println(s.verify(key,
		// BigInteger.valueOf(35057721672261313L)));
	}*/

}