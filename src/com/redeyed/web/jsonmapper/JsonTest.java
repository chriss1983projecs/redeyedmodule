package com.redeyed.web.jsonmapper;

import java.io.IOException;

import javax.jws.WebParam.Mode;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.redeyed.web.jsonmapper.JSONProperties.Backend;
import com.redeyed.web.jsonmapper.JSONProperties.Database;
import com.redeyed.web.jsonmapper.JSONProperties.Debugging;

public class JsonTest {
	private String username;
	private String password;
	private String databaseURL;
	private String dialect;
	private String driver;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDatabaseURL() {
		return databaseURL;
	}

	public void setDatabaseURL(String databaseURL) {
		this.databaseURL = databaseURL;
	}

	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	// GSON.fromJson(uiObj.getString("USER_DETAILS").toString(),
	// UserDetailsEntity.class);

	/*
	 * 
	 * "database": { "username": "root", "password": "admin", "databaseURL":
	 * "./db/repository/skatmanager", "dialect": "H2Dialect", "driver":
	 * "org.h2.Driver" }, "global": { "appName": "Skat Manager Haan-Gruiten",
	 * "version": "0.1", "width": "1000", "height": "800", "fullScreen": true,
	 * "theme": "BLACKEYE" }, "debugging": { "showErrorMessages": false },
	 * "security": { "activationEnable": false, "publicKey":
	 * "36559548014518483", "activationState": "DEMO", "activationKey": "",
	 * "secureKey":
	 * "9jVB5VoN3wUlnIVuHMTBU5VjfNlBHH2REVn/l8FrW2Wo4+a8O7sfk3nvlRuY18CqeJkc0UG0TfPyPWdH/tTIJchV7VKlh64/931b6zy8b04nbkSCYOTIJ9Labcp+RqZP"
	 * }, "backend": { "backendURL": "http://localhost", "backendPort": "8080",
	 * "backendConnectionURL": "TestBackend" }
	 */

	public static void main(String[] args) throws IOException {
		// Model.Database database = new Database();
		// Model.Debugging debugging = new Debugging();

		JSONProperties.getJSonProperty().getBackend().setBackendConnectionURL("www.dffffff");
		JSONProperties.getJSonProperty().getBackend().setBackendPort(666);
		JSONProperties.getJSonProperty().getBackend().setBackendURL("hhhh");

		JSONProperties.getJSonProperty().getDatabase().setDatabaseURL("URL");
		JSONProperties.getJSonProperty().getDatabase().setDialect("Langu");
		JSONProperties.getJSonProperty().getDatabase().setDriver("Driver");
		JSONProperties.getJSonProperty().getDatabase().setPassword("password");

		JSONProperties.getJSonProperty().getDebugging().setShowErrorMessages(true);
		
		JSONProperties.saveProperties();
//		Gson GSON = new GsonBuilder().create();

//		System.out.println(GSON.toJson(Model.getJSonProperty()));
		//String json = "{\"database\": {\"username\": \"root\",\"password\": \"admin\",\"databaseURL\": \"./db/repository/skatmanager\",\"dialect\": \"H2Dialect\",\"driver\": \"org.h2.Driver\"},\"global\": {\"appName\": \"Skat Manager Haan-Gruiten\",\"version\": \"0.1\",\"width\": \"1000\",\"height\": \"800\",\"fullScreen\": true,\"theme\": \"BLACKEYE\"},\"debugging\": {\"showErrorMessages\": false},\"security\": {\"activationEnable\": false,\"publicKey\": \"36559548014518483\",\"activationState\": \"DEMO\",\"activationKey\": \"\",\"secureKey\": \"9jVB5VoN3wUlnIVuHMTBU5VjfNlBHH2REVn/l8FrW2Wo4+a8O7sfk3nvlRuY18CqeJkc0UG0TfPyPWdH/tTIJchV7VKlh64/931b6zy8b04nbkSCYOTIJ9Labcp+RqZP\"},\"backend\": {\"backendURL\": \"http://localhost\",\"backendPort\": \"8080\",\"backendConnectionURL\": \"TestBackend\"}}";
		

//	Model model = 

	System.out.println(JSONProperties.getJSonProperty().toString());
	System.out.println(JSONProperties.getJSonProperty().getGlobal().getAppName());
}

}
