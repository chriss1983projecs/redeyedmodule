package com.redeyed.web.jsonmapper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JSONProperties {
	private Backend backend;
	private Database database;
	private Debugging debugging;
	private Security security;
	private Global global;
	private static JSONProperties model;

	private static Gson GSON = new GsonBuilder().create();

	public static JSONProperties getJSonProperty() {
		if (model == null) {
			model = new JSONProperties();

			ClassLoader classLoader = new JSONProperties().getClass().getClassLoader();
			File file = new File(classLoader.getResource("properties.json").getFile());

			System.out.println("File Found : " + file.exists());

			String content = null;
			try {
				content = new String(Files.readAllBytes(file.toPath()));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			model = GSON.fromJson(content, JSONProperties.class);
			System.out.println(content);

			System.out.println(model.getBackend().backendConnectionURL);
		}
		return model;
	}

	public static void saveProperties() throws JsonProcessingException, IOException {
		JsonFactory jsonFactory;
		FileOutputStream file;
		JsonGenerator jsonGen;

		jsonFactory = new JsonFactory();
		file = new FileOutputStream(new File("resources/properties.json"));
		jsonGen = jsonFactory.createJsonGenerator(file, JsonEncoding.UTF8);
		jsonGen.setCodec(new ObjectMapper());
		jsonGen.writeObject(JSONProperties.getJSonProperty());
	}

	private JSONProperties() {
		this.backend = new Backend();
		this.database = new Database();
		this.debugging = new Debugging();
		this.security = new Security();
		this.global = new Global();

	}

	public Backend getBackend() {
		return backend;
	}

	public void setBackend(Backend backend) {
		this.backend = backend;
	}

	public Database getDatabase() {
		return database;
	}

	public void setDatabase(Database database) {
		this.database = database;
	}

	public Debugging getDebugging() {
		return debugging;
	}

	public void setDebugging(Debugging debugging) {
		this.debugging = debugging;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public Global getGlobal() {
		return global;
	}

	public void setGlobal(Global global) {
		this.global = global;
	}

	@Override
	public String toString() {
		return "JSONProperties: \n BACKEND_CLASS=" + backend.toString() + ",\n DATABASE_CLASS=" + database.toString()
				+ ",\n DEBUGGING_CLASS=" + debugging.toString() + ",\n SECURITY_CLASS=" + security.toString()
				+ ",\n GLOBAL_CLASS=" + global.toString();
	}

	public class Backend {
		private String backendURL;
		private Integer backendPort;
		private String backendConnectionURL;
		private String requestType;

		public String getRequestType() {
			return requestType;
		}

		public void setRequestType(String requestType) {
			this.requestType = requestType;
		}

		public String getBackendURL() {
			return backendURL;
		}

		public void setBackendURL(String backendURL) {
			this.backendURL = backendURL;
		}

		public Integer getBackendPort() {
			return backendPort;
		}

		public void setBackendPort(Integer backendPort) {
			this.backendPort = backendPort;
		}

		public String getBackendConnectionURL() {
			return backendConnectionURL;
		}

		public void setBackendConnectionURL(String backendConnectionURL) {
			this.backendConnectionURL = backendConnectionURL;
		}

		@Override
		public String toString() {
			return "Backend [backendURL=" + backendURL + ", backendPort=" + backendPort + ", backendConnectionURL="
					+ backendConnectionURL + "]";
		}
	}

	public class Database {
		private String username;
		private String password;
		private String databaseURL;
		private String dialect;
		private String driver;

		public Database() {
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String userName) {
			this.username = userName;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getDatabaseURL() {
			return databaseURL;
		}

		public void setDatabaseURL(String databaseURL) {
			this.databaseURL = databaseURL;
		}

		public String getDialect() {
			return dialect;
		}

		public void setDialect(String dialect) {
			this.dialect = dialect;
		}

		public String getDriver() {
			return driver;
		}

		public void setDriver(String driver) {
			this.driver = driver;
		}

		@Override
		public String toString() {
			return "Database [username=" + username + ", password=" + password + ", databaseURL=" + databaseURL
					+ ", dialect=" + dialect + ", driver=" + driver + "]";
		}
	}

	public class Debugging {
		private Boolean showErrorMessages;

		public Debugging() {
			// TODO Auto-generated constructor stub
		}

		public Boolean isShowErrorMessages() {
			return showErrorMessages;
		}

		public void setShowErrorMessages(Boolean showErrorMessages) {
			this.showErrorMessages = showErrorMessages;
		}

		@Override
		public String toString() {
			return "Debugging [showErrorMessages=" + showErrorMessages + "]";
		}
	}

	public class Security {

		private Boolean activationEnable;
		private String publicKey;
		private String activationState;
		private String activationKey;
		private String secureKey;

		public Boolean getActivationEnable() {
			return activationEnable;
		}

		public void setActivationEnable(Boolean activationEnable) {
			this.activationEnable = activationEnable;
		}

		public String getPublicKey() {
			return publicKey;
		}

		public void setPublicKey(String publicKey) {
			this.publicKey = publicKey;
		}

		public String getActivationState() {
			return activationState;
		}

		public void setActivationState(String activationState) {
			this.activationState = activationState;
		}

		public String getActivationKey() {
			return activationKey;
		}

		public void setActivationKey(String activationKey) {
			this.activationKey = activationKey;
		}

		public String getSecureKey() {
			return secureKey;
		}

		public void setSecureKey(String secureKey) {
			this.secureKey = secureKey;
		}

		@Override
		public String toString() {
			return "Security [activationEnable=" + activationEnable + ", publicKey=" + publicKey + ", activationState="
					+ activationState + ", activationKey=" + activationKey + ", secureKey=" + secureKey + "]";
		}
	}

	public class Global {

		private String appName;
		private String version;
		private String width;
		private String height;
		private Boolean fullScreen;
		private String theme;

		public String getAppName() {
			return appName;
		}

		public void setAppName(String appName) {
			this.appName = appName;
		}

		public String getVersion() {
			return version;
		}

		public void setVersion(String version) {
			this.version = version;
		}

		public String getWidth() {
			return width;
		}

		public void setWidth(String width) {
			this.width = width;
		}

		public String getHeight() {
			return height;
		}

		public void setHeight(String height) {
			this.height = height;
		}

		public Boolean getFullScreen() {
			return fullScreen;
		}

		public void setFullScreen(Boolean fullScreen) {
			this.fullScreen = fullScreen;
		}

		public String getTheme() {
			return theme;
		}

		public void setTheme(String theme) {
			this.theme = theme;
		}

		@Override
		public String toString() {
			return "Global [appName=" + appName + ", version=" + version + ", width=" + width + ", height=" + height
					+ ", fullScreen=" + fullScreen + ", theme=" + theme + "]";
		}

	}
}
