package com.redeyed.web.service;

import javax.servlet.http.HttpServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpServerErrorException;

@Service
public class DefaultService  {

	private static final Logger logger = LoggerFactory.getLogger(DefaultService.class);

	public String getDesc() {

		logger.debug("getDesc() is executed!");

		return "Gradle + Spring MVC Hello World Example";

	}

	public String getTitle(String name) {

		logger.debug("getTitle() is executed! $name : {}", name);
		

		if(StringUtils.isEmpty(name)){
			return "index.jsp";
		}else{
			return "Hello " + name;
		}
		
	}
}