package com.redeyed.web.database;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.reflections.Reflections;

import com.redeyed.web.jsonmapper.JSONProperties;
import com.redeyed.web.utils.JSONPropertiesManager;
import com.redeyed.web.utils.ReflectionUtils;

public class HibernateConfigUtil {
	final static Logger logger = Logger.getLogger(HibernateConfigUtil.class);
	// Property based configuration
	private static SessionFactory sessionFactory;
	private static ServiceRegistry serviceRegistry;
	private static List<Object> beans;
	private static HibernateConfigUtil instance;

	/**
	 * XML Hibernate Configuration
	 * @return
	 */
//	@SuppressWarnings("unused")
//	@Deprecated
//	private static SessionFactory buildSessionFactory() {
//		try {
//			// Create the SessionFactory from hibernate.cfg.xml
//			Configuration configuration = new Configuration();
//			configuration.configure("hibernate.cfg.xml");
//			logger.info("Hibernate Configuration loaded");
//
//			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
//					.applySettings(configuration.getProperties()).build();
//			logger.info("Hibernate serviceRegistry created");
//
//			SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
//
//			return sessionFactory;
//		} catch (Throwable ex) {
//			// Make sure you log the exception, as it might be swallowed
//			logger.error("Initial SessionFactory creation failed." + ex.getMessage());
//			throw new ExceptionInInitializerError(ex);
//		}
//	}
//
	private SessionFactory buildSessionConfigFactory() {
		try {
//			Map<String, Object> properties = StartSkatManager.properties.getMap("database");

//			System.out.println(JSONProperties.getInstance().getMap("database").get("dialect"));
			Configuration configuration = new Configuration();

			// Create Properties, can be read from property files too
			Properties props = new Properties();

			props.put("hibernate.dialect", "org.hibernate.dialect." + JSONProperties.getJSonProperty().getDatabase().getDialect());
			props.put("hibernate.connection.driver_class", JSONProperties.getJSonProperty().getDatabase().getDriver()); // org.h2.Driver
			props.put("hibernate.connection.url", "jdbc:mysql:" + JSONProperties.getJSonProperty().getDatabase().getDatabaseURL()); // ./test
																								// jdbc:h2:./db/repository/test
			props.put("hibernate.connection.username", JSONProperties.getJSonProperty().getDatabase().getUsername());
			props.put("hibernate.connection.password", JSONProperties.getJSonProperty().getDatabase().getPassword());
			props.put("hibernate.current_session_context_class", "thread"); //
			props.put("hibernate.hbm2ddl.auto", "update");

			configuration.setProperties(props);
			logger.info("Hibernate Configuration loaded");

			// we can set mapping file or class with annotation
			// addClass(Employee1.class) will look for resource
			// com/journaldev/hibernate/model/Employee1.hbm.xml (not good)
			Reflections reflections = new Reflections();

			Set<Class> allClasses = ReflectionUtils.getAllClasses("com.redeyed.web.model");
			if (allClasses != null) {
				for (Class<?> class1 : allClasses) {
					configuration.addAnnotatedClass(class1);
					System.out.println(class1.getName());
				}
			} else {
				System.out.println("ERROR CLASSES");
			}

			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
					.applySettings(configuration.getProperties()).build();
			logger.debug("Hibernate Java Config serviceRegistry created");
			SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);

			return sessionFactory;
		} catch (Throwable ex) {
			logger.error("Initial SessionFactory creation failed." + ex.getMessage());
			throw new ExceptionInInitializerError(ex);
		}
	}

	public SessionFactory getSessionConfigFactory() {
		logger.info("getSessionConfigFactory()");
		if (sessionFactory == null) {
			sessionFactory = buildSessionConfigFactory();

		}
		return sessionFactory;
	}

	private Set<Class> getAllClasses(String pckgname) {
		try {
			Set<Class> classes = new HashSet<Class>();
			// Get a File object for the package
			File directory = null;
			try {
				directory = new File(Thread.currentThread().getContextClassLoader()
						.getResource(pckgname.replace('.', '/')).getFile());
			} catch (NullPointerException x) {
				logger.error("Nullpointer Exception - " + pckgname + " does not appear to be a valid package");
				System.out.println("Nullpointer");
				throw new ClassNotFoundException(pckgname + " does not appear to be a valid package");
			}
			if (directory.exists()) {
				String[] files = directory.list();
				for (int i = 0; i < files.length; i++) {
					if (files[i].endsWith(".class")) {
						// removes the .class extension
						classes.add(Class.forName(pckgname + '.' + files[i].substring(0, files[i].length() - 6)));
					}
				}
			} else {
				logger.debug("Directory does not exist");
				throw new ClassNotFoundException(pckgname + " does not appear to be a valid package");
			}

			return classes;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void close() throws Exception {
		if (serviceRegistry != null) {
			StandardServiceRegistryBuilder.destroy(serviceRegistry);
		}
	}

	public static HibernateConfigUtil getInstance()  {
		if(instance == null) {
			instance = new HibernateConfigUtil();
		}
		return instance;
	}
}
