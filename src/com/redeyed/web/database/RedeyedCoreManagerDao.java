package com.redeyed.web.database;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.redeyed.web.database.query.base.components.GroupBy;
import com.redeyed.web.database.query.base.components.OrderBy;
import com.redeyed.web.database.query.base.components.Pagination;

//@Repository
public class RedeyedCoreManagerDao implements RedeyedCoreManager {
	final static Logger logger = Logger.getLogger(RedeyedCoreManagerDao.class);
	private static RedeyedCoreManagerDao dataBaseUtil;
	
	private SessionFactory sessionFactory;

	public RedeyedCoreManagerDao() {
		logger.info("Init DateBaseUtil");
		this.sessionFactory = HibernateConfigUtil.getInstance().getSessionConfigFactory();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getAllEntities(Class<T> clazz, List<String> conditions, OrderBy orderBy, Pagination pagination, GroupBy groupBy) {
		try {
			String queryString = "FROM " + clazz.getSimpleName() + getConditions(conditions) + getGroupBy(groupBy) + getOrderBy(orderBy);
			 
		 
			Query q = this.sessionFactory.openSession().createQuery(queryString);
			setPagination(q, pagination, queryString);
			return q.list();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return new ArrayList<T>();
		}
	}

	private String getGroupBy(GroupBy groupBy) {
		if (groupBy != null) {
			return " GROUP BY " + groupBy.getVariableName() + " ";
		} else {
			return "";
		}
	}

	private void setPagination(Query query, Pagination pagination, String queryString) {
		if (pagination != null && pagination.getPageNumber() != null && pagination.getPageSize() != null) {
			query.setFirstResult((pagination.getPageNumber() - 1) * pagination.getPageSize());
			query.setMaxResults(pagination.getPageSize());
			pagination.setTotalRows(getRowCount(queryString));
		}
	}

	private String getOrderBy(OrderBy orderBy) {
		if (orderBy != null) {
			return " ORDER BY " + orderBy.getVariableName() + " " + (orderBy.isSortAsc() == true ? "ASC" : "DESC");
		} else {
			return "";
		}
	}

	private String getConditions(List<String> conditions) {
		if (conditions != null) {
			StringBuilder conditionsStr = new StringBuilder(" WHERE ");
			for (String condition : conditions)
				conditionsStr.append(condition).append(" AND ");
			return conditionsStr.substring(0, conditionsStr.length() - 5);
		}
		return "";
	}

	@Override
	public boolean saveOrUpdate(Object object) {
		try {
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.saveOrUpdate(object);
			session.getTransaction().commit();
		} catch (Exception ex) {
//			logger.error(ex.getCause().getMessage(), ex);
		}
		return true;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T getEntity(Class<T> clazz, int entityId) {
		try {
			return (T) this.sessionFactory.openSession().get(clazz, entityId);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return null;
		}
	}

	@Override
	public boolean deleteEntity(Class<?> clazz, int entityId) {
		try {
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.delete(getEntity(clazz, entityId));
			session.getTransaction().commit();
			 
			return true;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return false;
		}
	}

	@Override
	public boolean deleteEntity(Object object) {
		try {
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			session.delete(object);
			session.getTransaction().commit();
			return true;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return false;
		}
	}

	private <T> int getRowCount(String queryString) {
		String query = "SELECT COUNT(*) " + queryString;
		return ((Long) this.sessionFactory.openSession().createQuery(query).uniqueResult()).intValue();
	}

	@Override
	public <T> int getRowCount(Class<T> clazz) {
		return getRowCount("FROM " + clazz.getSimpleName());
	}
	
	public static RedeyedCoreManagerDao getDataBaseUtil() {
		if(dataBaseUtil == null) {
			dataBaseUtil = new RedeyedCoreManagerDao();
		}
		return dataBaseUtil;
	}
}

