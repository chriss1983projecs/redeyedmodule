package com.redeyed.web.database;

import java.util.List;

import com.redeyed.web.database.query.base.components.GroupBy;
import com.redeyed.web.database.query.base.components.OrderBy;
import com.redeyed.web.database.query.base.components.Pagination;

public interface RedeyedCoreManager {

	<T> List<T> getAllEntities(Class<T> clazz, List<String> conditions, OrderBy orderBy, Pagination pagination,
			GroupBy groupBy);

	boolean saveOrUpdate(Object entityObject);

	<T> T getEntity(Class<T> clazz, int entityId);

	boolean deleteEntity(Class<?> clazz, int entityId);

	boolean deleteEntity(Object object);

	<T> int getRowCount(Class<T> clazz);
	
}