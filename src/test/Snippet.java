package test;

import java.util.ArrayList;
import java.util.List;

import com.redeyed.web.database.RedeyedCoreManager;
import com.redeyed.web.database.RedeyedCoreManagerDao;
import com.redeyed.web.model.UserDetailsEntity;

public class Snippet {
	public static void main(String[] args) {
			List<String> conditions = new ArrayList<String>();
			conditions.add("FIRSTNAME = 'Günter'");
			RedeyedCoreManager coreManager = new RedeyedCoreManagerDao();
		
			List<UserDetailsEntity> activationKeyDetails = coreManager.getAllEntities(UserDetailsEntity.class, conditions, null, null, null);
			
			for(UserDetailsEntity d : activationKeyDetails) {
				System.out.println(d.getFirstName());
				System.out.println(d.getLastName());
			}
			
			UserDetailsEntity detailsEntity = new UserDetailsEntity();
			
			detailsEntity.setFirstName("Günter");
			detailsEntity.setLastName("Mielke");
			detailsEntity.setEmail("ich@du.de");
			
//			DataBaseUtil.getDataBaseUtil().saveOrUpdate(detailsEntity);
			
			System.out.println(coreManager.deleteEntity(UserDetailsEntity.class, 8));
			
			System.out.println(coreManager.getRowCount(UserDetailsEntity.class));
		}
}

