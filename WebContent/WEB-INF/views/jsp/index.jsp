<!DOCTYPE html>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="redeyed" uri="http://redeyed.de/redeyed/core"%>


<html lang="de">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<redeyed:CSS />
<redeyed:JS />

<title>Willkommen auf meiner Seine</title>

</head>

<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="index">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Redeyed.de</a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Home</a></li>
					<li><a class="btn btn-link-1 launch-modal" href="#" data-target="#modal-login" data-toggle="modal">Page 2</a></li>
					<li><a href="#">Page 3</a></li>
				</ul>
				<br>
				<hr class="prettyline">
			</div>
			<form class="navbar-form navbar-right" role="login">
				<button class="btn btn-primary btn-md" data-target="#modal-login" role="button" data-toggle="modal">Sign In/Register</button>
			</form>

		</div>
	</nav>

	<!-- nav nabvar-nav -->
	<div class="jumbotron">
		<div class="container">
			<h1>${title}</h1>
			<p>
				<c:set var="firstName" scope="session" value="${peter}" />
				<c:out value="${firstName}" />

				<%-- 					<c:set var="name_${player.firstName}" value="${player.zipCode}" scope="session"/> --%>
				<c:set var="firstName" value="${player.zipCode}" />
			</p>

			<c:if test="${not empty msg}">
				Hello ${msg}
			</c:if>

			<c:if test="${empty msg}">
				Welcome Welcome!
			</c:if>
			<p>${player.firstName}${player.lastName}</p>
			<p>${player.zipCode}${player.city}</p>

			<a href="#" data-target="#InputForm" role="button" data-toggle="modal">Login</a>
			<%-- 			<c:out value="${firstName}" --%>
			you typed:
			<c:out value="${name}" />
			<p>
				<a class="btn btn-primary btn-lg" data-toggle="collapse" data-target="#InputForm" role="button">Learn more</a>
			</p>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h2>Heading</h2>
				<p>ABC</p>
				<p>
					<a class="btn btn-default" href="#" role="button">View details</a>
				</p>
			</div>
			<div class="col-md-4">
				<h2>Heading</h2>
				<p>ABC</p>
				<p>
					<a class="btn btn-default" href="#" role="button">View details</a>
				</p>
			</div>
			<div class="col-md-4">
				<h2>Heading</h2>
				<p>ABC</p>
				<p>
					<a class="btn btn-default" href="#" role="button">View details</a>
				</p>
			</div>
		</div>


		<hr>
		<footer>
			<p>&copy; Christian Richter 2017</p>
		</footer>
	</div>


</body>

<div class="modal fade" id="InputForm" tabindex="-1" role="main" aria-labelledby="addLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <form class="form-vertical" action="Home/AddProduct" role="form" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>Add product:</h3>
                </div>
                <div class="modal-body">
                    <fieldset>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="firstName">ASIN </label>
                                <input type="text" class="form-control"
                                       id="asin" name="asin" placeholder="Amazon ASIN">
                            </div>
                            <div class="col-md-6">
                                <label for="firstName">EAN code </label>
                                <input type="text" class="form-control"
                                       id="eanCode" name="eanCode" placeholder="EAN code">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class="col-md-6">
                                    <a href="#" action="/Product/GetASIN/" class="btn btn-primary">Get ASIN</a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" data-toggle="toggle" name="publishedAmazon">publish on Amazon
                                    </label>
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="productDescription">Product description </label>
                            <textarea class="form-control" id="productDescription" name="productDescription" placeholder="Please enter a product description"></textarea>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label for="salesPrice">Seller </label>
                                @Html.DropDownList("Supplier", ViewBag.EnumList as SelectList, new { @class = "form-control" })

                            </div>
                            <div class="col-md-6">
                                <label for="salesPrice">Sales price (including VAT) </label>
                                <input class="form-control" id="salesPrice" name="salesPrice" placeholder="Sales price"></input>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label for="stock">Stock </label>
                                <input type="text" class="form-control"
                                       id="stock" name="stock" placeholder="Please enter stock">
                                @Html.ValidationMessageFor(x => x.m_Stock, "", new { @class = "text-danger" })
                            </div>
                            <div class="col-md-4">
                                <label for="minPrice">Minimum price </label>
                                <input type="text" class="form-control"
                                       id="minPrice" name="minPrice" placeholder="Minimum price">
                            </div>
                            <div class="col-md-4">
                                <label for="maxPrice">Maximum price </label>
                                <input type="text" class="form-control"
                                       id="maxPrice" name="maxPrice" placeholder="Maximum price">
                            </div>
                        </div>
                    </fieldset>

                    <div class="modal-footer">
                        <span class='label label-info' id="upload-file-info"></span>
                        <input type="submit" formaction="/Product/SaveOrEditProduct/" value="Add Product" id="submit" name="submit" class="btn btn-primary">
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>

<!-- MODAL -->
<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="modal-login-label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h3 class="modal-title" id="modal-login-label">Login to our site</h3>
				<p>Enter your username and password to log on:</p>
			</div>

			<div class="modal-body">

				<form role="form" action="" method="post" class="login-form">
					<div class="form-group">
						<label class="sr-only" for="form-username">Username</label> <input type="text" name="form-username" placeholder="Username..." class="form-username form-control" id="form-username">
					</div>
					<div class="form-group">
						<label class="sr-only" for="form-password">Password</label> <input type="password" name="form-password" placeholder="Password..." class="form-password form-control" id="form-password">
					</div>
					<button type="submit" class="btn">Sign in!</button>
				</form>

			</div>

		</div>
	</div>
</div>

</html>